(define (cube x)
  (* x x x))

(define (cbrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (cbrt-iter (improve guess x)
                 x)))

(define (improve guess x)
  (/ (+ (/ x
           (* guess guess))
        (* 2 guess))
     3))

(define (good-enough? guess x)
  (< (abs (- guess (improve guess x)))
     0.001))

(define (cbrt x)
  (cbrt-iter 1.0 x))

(define (cbrt-impr x)
  (define (cbrt-iter guess)
    (if (good-enough? guess)
        guess
        (cbrt-iter (improve guess))))
  (define (improve guess)
    (/ (+ (/ x
             (* guess guess))
          (* 2 guess))
       3))
  (define (good-enough? guess)
    (< (abs (- guess (improve guess)))
       0.001))
  (cbrt-iter 1.0))
