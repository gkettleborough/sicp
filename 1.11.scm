;;; f(n) = | n if n < 3;
;;;        | f(n-1) + 2f(n-2) + 3f(n-3) if n >= 3.

(define (recursive-f n)
  (if (< n 3)
      n
      (+ (recursive-f (- n 1))
         (* 2 (recursive-f (- n 2)))
         (* 3 (recursive-f (- n 3))))))

(define (iterative-f n)
  (define (iter n1 n2 n3)))
